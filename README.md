# Dynamic Struct

**這是一個非必要的函式庫，你可以調整你的習慣來達成它**
這個函式庫用來幫助需要頻繁增刪欄位的物件
避免其進入 V8 字典模式降低效能

**This is a non-essential library, you can adjust your habits to reach it.**
This library is used to help objects that need to be frequently added or deleted.
Avoid entering V8 dictionary mode to reduce performance.

## 使用須知 Usage Notice

- 必須初始化物件所有欄位
- 值為 `undefined` 的欄位視為不存在
- 繼承 `DStruct` 為非必需的

- must initialize all fields of the object
- A field with a value of `undefined` is considered to be absent
- Inherit `DStruct` is not required

## API

這是一個非常單純的函式庫，所以我不寫 API 文件
This is a very simple library, so I don't write API files.

## 效能測試 Benchmark

```
{}	Origin Object
DS	Dynamic Struct
DS=	Dynamic Struct use =undefined to delete a key

  benchmark prop8
{}: i=10000000 time=31 ops=322580645
DS: i=10000000 time=224 ops=44642857
    ✓ new (259ms)
{}: i=10000000 time=60 ops=166666666
DS: i=10000000 time=62 ops=161290322
    ✓ set prop@fast mode (124ms)
{}: i=10000000 time=61 ops=163934426
DS: i=10000000 time=60 ops=166666666
    ✓ get prop@fast mode (121ms)
{}: i=10000000 time=428 ops=23364485
DS: i=10000000 time=706 ops=14164305
DS=: i=10000000 time=61 ops=163934426
    ✓ delete prop@fast mode (1196ms)
{}: i=10000000 time=128 ops=78125000
DS: i=10000000 time=61 ops=163934426
    ✓ set prop@dict mode (189ms)
{}: i=10000000 time=198 ops=50505050
DS: i=10000000 time=61 ops=163934426
    ✓ get prop@dict mode (259ms)
{}: i=10000000 time=1074 ops=9310986
DS: i=10000000 time=777 ops=12870012
DS=: i=10000000 time=60 ops=166666666
    ✓ delete prop@dict mode (1913ms)
Object.keys: i=10000000 time=274 ops=36496350
DStruct.keys: i=10000000 time=1901 ops=5260389
    ✓ .keys() (2175ms)
Object.values: i=10000000 time=2628 ops=3805175
DStruct.values: i=10000000 time=2518 ops=3971405
    ✓ .values() (5146ms)
Object#hasOwnProperty exists: i=10000000 time=146 ops=68493150
Object#hasOwnProperty not exists: i=10000000 time=182 ops=54945054
DStruct#has exists: i=10000000 time=58 ops=172413793
DStruct#has not exists: i=10000000 time=64 ops=156250000
DStruct#has undefined key: i=10000000 time=58 ops=172413793
    ✓ #has() (509ms)
DStruct#hasKey exists: i=10000000 time=149 ops=67114093
DStruct#hasKey not exists: i=10000000 time=184 ops=54347826
    ✓ #hasKey() (333ms)
```
