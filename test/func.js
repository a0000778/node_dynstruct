'use strict';
const assert=require('assert');
const {DStruct}=require('..');

class S extends DStruct {
	constructor(){
		super();
		this.a=1;
		this.b=undefined;
	}
}

describe('func',function(){
	it('new DStruct',function(){
		new S();
	});
	it('DStruct.assign',function(){
		const t={'a': 1,'b': undefined};
		const o=DStruct.assign(
			t,
			{'b': 2,'c': 3}
		);
		assert.strictEqual(t,o);
		assert.deepStrictEqual(o,{'a': 1,'b': 2});
	});
	it('DStruct.drop',function(){
		const o={'a':1,'b':2,'c':3};
		DStruct.drop(o,'c','d');
		assert.deepStrictEqual(o,{'a': 1,'b': 2,'c': undefined});
	});
	it('DStruct.keys',function(){
		const k=DStruct.keys({'a':1,'b':undefined});
		assert.deepStrictEqual(k,['a']);
	});
	it('DStruct.values',function(){
		const v=DStruct.values({'a':1,'b':undefined});
		assert.deepStrictEqual(v,[1]);
	});
	it('DStruct#drop',function(){
		const o=new S();
		o.drop('a');
		assert.strictEqual(o.a,undefined);
	});
	it('DStruct#has',function(){
		const o=new S();
		assert.strictEqual(o.has('a'),true);
		assert.strictEqual(o.has('b'),false);
		assert.strictEqual(o.has('c'),false);
	});
	it('DStruct#hasKey',function(){
		const o=new S();
		assert.strictEqual(o.hasKey('a'),true);
		assert.strictEqual(o.hasKey('b'),true);
		assert.strictEqual(o.hasKey('c'),false);
	});
});