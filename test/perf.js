'use strict';
const {DStruct}=require('..');

class Prop8 extends DStruct {
	constructor(){
		super();
		this.p0=undefined;
		this.p1=undefined;
		this.p2=undefined;
		this.p3=undefined;
		this.p4=undefined;
		this.p5=undefined;
		this.p6=undefined;
		this.p7=undefined;
	}
}
function makeDictObject(){
	const o={};
	o.p0=0;
	o.p1=1;
	o.p2=2;
	o.p3=3;
	o.p4=4;
	delete o.p0;
	o.p5=5;
	delete o.p1,o.p2;
	return o;
}
function makeDSObject(){
	const o=new Prop8();
	o.p0=0;
	o.p1=1;
	o.p2=2;
	o.p3=3;
	o.p4=4;
	o.drop('p0');
	o.p5=5;
	o.drop('p1','p2');
	return o;
}
function benchmark(name,func,i=10000000){
	let n=i;
	const start=Date.now();
	do{
		func();
	}while(--n);
	const end=Date.now();
	const time=end-start;
	console.log(`${name}: i=${i} time=${time} ops=${Math.floor(i*1000/time)}`);
}

describe('benchmark prop8',function(){
	this.timeout(10000);
	it('new',function(){
		benchmark('{}',function(){
			{};
		});
		benchmark('DS',function(){
			new Prop8();
		});
	});
	it('set prop@fast mode',function(){
		const o={};
		const c=new Prop8();
		benchmark('{}',function(){
			o.p0=0;
		});
		benchmark('DS',function(){
			c.p0=0;
		});
	});
	it('get prop@fast mode',function(){
		const o={};
		const c=new Prop8();
		benchmark('{}',function(){
			o.p0=0;
			o.p0;
		});
		benchmark('DS',function(){
			c.p0=0;
			c.p0;
		});
	});
	it('delete prop@fast mode',function(){
		const o={};
		const c=new Prop8();
		benchmark('{}',function(){
			o.p0=1;
			delete o.p0;
		});
		benchmark('DS',function(){
			c.p0=1;
			c.drop('p0');
		});
		benchmark('DS=',function(){
			c.p0=1;
			c.p0=undefined;
		});
	});
	it('set prop@dict mode',function(){
		const o=makeDictObject();
		const c=makeDSObject();
		benchmark('{}',function(){
			o.p0=0;
		});
		benchmark('DS',function(){
			c.p0=0;
		});
	});
	it('get prop@dict mode',function(){
		const o=makeDictObject();
		const c=makeDSObject();
		benchmark('{}',function(){
			o.p0=0;
			o.p0;
		});
		benchmark('DS',function(){
			c.p0=0;
			c.p0;
		});
	});
	it('delete prop@dict mode',function(){
		const o=makeDictObject();
		const c=makeDSObject();
		benchmark('{}',function(){
			o.p0=1;
			delete o.p0;
		});
		benchmark('DS',function(){
			c.p0=1;
			c.drop('p0');
		});
		benchmark('DS=',function(){
			c.p0=1;
			c.p0=undefined;
		});
	});
	it('.keys()',function(){
		const c=makeDSObject();
		benchmark('Object.keys',function(){
			Object.keys(c);
		});
		benchmark('DStruct.keys',function(){
			DStruct.keys(c);
		});
	});
	it('.values()',function(){
		const c=makeDSObject();
		benchmark('Object.values',function(){
			Object.values(c);
		});
		benchmark('DStruct.values',function(){
			DStruct.values(c);
		});
	});
	it('#has()',function(){
		const c=makeDSObject();
		benchmark('Object#hasOwnProperty exists',function(){
			c.hasOwnProperty('p5');
		});
		benchmark('Object#hasOwnProperty not exists',function(){
			c.hasOwnProperty('p9');
		});
		benchmark('DStruct#has exists',function(){
			c.has('p5');
		});
		benchmark('DStruct#has not exists',function(){
			c.has('p0');
		});
		benchmark('DStruct#has undefined key',function(){
			c.has('p9');
		});
	});
	it('#hasKey()',function(){
		const c=makeDSObject();
		benchmark('DStruct#hasKey exists',function(){
			c.hasKey('p5');
		});
		benchmark('DStruct#hasKey not exists',function(){
			c.hasKey('p9');
		});
	});
});