'use strict';
class DStruct{
	static assign(target,...sources){
		const existsKeys=Object.keys(target);
		let s,k,sk;
		for(s of sources){
			sk=DStruct.keys(s);
			for(k of sk){
				if(!existsKeys.includes(k))
					continue;
				target[k]=s[k];
			}
		}
		return target;
	}
	static drop(obj,...keys){
		const existsKeys=Object.keys(obj);
		for(let i=0;i<keys.length;i++){
			if(!existsKeys.includes(keys[i]))
				continue;
			obj[keys[i]]=undefined;
		}
	}
	static keys(obj){
		const keys=Object.keys(obj);
		const ret=[];
		for(let i=0;i<keys.length;i++){
			if(obj[keys[i]]===undefined)
				continue;
			ret.push(keys[i]);
		}
		return ret;
	}
	static values(obj){
		const keys=DStruct.keys(obj);
		const ret=new Array(keys.length);
		for(let i=0;i<keys.length;i++)
			ret[i]=obj[keys[i]];
		return ret;
	}
	drop(...keys){
		DStruct.drop(this,...keys);
	}
	has(key){
		return this[key]!==undefined;
	}
	hasKey(key){
		return Object.prototype.hasOwnProperty.call(this,key);
	}
}
module.exports={DStruct};
